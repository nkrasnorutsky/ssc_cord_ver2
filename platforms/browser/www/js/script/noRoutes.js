var map;

function setErrorPageTitle(name, comment){
  $('.name').text((name).toUpperCase());
  $('.desc').text(comment);
}

function setMap(coords){

  var iconFeature = new ol.Feature({
    geometry: new ol.geom.Point(coords),
    population: 4000,
    rainfall: 500
  });
  
  var iconStyle = new ol.style.Style({
    image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
      anchor: [0.5, 100],
      anchorXUnits: 'fraction',
      anchorYUnits: 'pixels',
      opacity: 1,
      src: 'img/location.png'
    }))
  });
  
  iconFeature.setStyle(iconStyle);
  
  var vectorSource = new ol.source.Vector({
    features: [iconFeature]
  });
  
  var vectorLayer = new ol.layer.Vector({
    source: vectorSource
  });
  
  var rasterLayer = new ol.layer.Tile({
    source: new ol.source.TileJSON({
      url: 'http://api.tiles.mapbox.com/v3/mapbox.geography-class.jsonp'
    })
  });

       map = new ol.Map({
       view: new ol.View({
          center: coords,
          zoom: 16
        }),
      controls: ol.control.defaults().extend([
          new ol.control.FullScreen({
            source: 'fullscreen'
          })
        ]),
        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          }), rasterLayer, vectorLayer
        ],
        target: 'map',
      });
}

function handleErrorPage(data){
  var coords = ol.proj.transform([data.stops[id].lon, data.stops[id].lat], 'EPSG:4326', 'EPSG:3857');
  setMap(coords);
  setErrorPageTitle(data.stops[id].name, data.stops[id].comment);
}

function ErrorPageRequest(){
  $.ajax({
    type: "POST",
    crossOrigin: true,
    url: "http://passenger.vistar.su/VPArrivalServer/stoplist",
    data: JSON.stringify({regionId: 36}),
    success: handleErrorPage,
    contentType: "application/json;charset=UTF-8",
    dataType: "json",
    error: error
  });
}