var markers = [];
var primaryArray = [];
var info = [];
var availableBuses = [];
var maxInfoHeight = window.innerHeight - 150 - 65;

function getMinutes (sec) {
    return Math.floor(sec / 60);
}

function clearScreen(){
     $('.informations').empty();
}

function setArrivalTimeInformation(pr){
    var j = 0;
    info = [];
    availableBuses = [];
    for(var i = 0; i < pr.length - 1; i++){
        if(pr[i].busRoute == pr[i + 1].busRoute){
            info[j] = {
                busRoute : pr[i].busRoute,
                nearTime : pr[i].time,
                nextTime : pr[i + 1].time,
                order : null
            }
            availableBuses[j] = pr[i].busRoute;
            j++;
        }   
    }
    if (availableBuses.length == 0){ 
        for(var i = 0; i < pr.length; i++){ 
            info[i] = {
                busRoute : pr[i].busRoute,
                nearTime : pr[i].time,
                nextTime : undefined,
                order : null
            }
        }
    } 

    var i, g, k;

    for(g = 0; g < pr.length; g++){
        k = 0;
        for(i = 0; i < availableBuses.length; i++){
            if(pr[g].busRoute != availableBuses[i]) {
                k++;
            }
        }
        if(k == availableBuses.length){
            info[j] = {
                busRoute : pr[g].busRoute,
                nearTime : pr[g].time,
                nextTime : undefined,
                order : null
            }
            j++;
        }
    }

}

function setInfo(){
    clearScreen();
    sortBusRoutes();
    var infoHeight = 99 * info.length;
    if(infoHeight < maxInfoHeight) {  
            putInfo(info);
    } else {
        putInfo(info);
        $('.info').css({
            'position': 'absolute',
            'left': '0',
            'right': '65%',
            'margin-top': '225px',
            'height': '99px' 
        });
        $('.icons').css({
            'background-color': '#fefefe',
            'position': 'relative',
            'z-index': '99'
        });
        $('.sidepanel-titel').css({
            'position': 'relative',
            'z-index': '99'
        });
        $(function(){
            $('.informations').verticalMarquee();
        });
    }
}

function busMovement(data){
    primaryArray = [];
    markers = [];
    for(var i = 0; i < data.busArrival[0].arrivals.length; i++){
        markers[i] = {
            busRoute : data.busArrival[0].arrivals[i].busRoute, 
            coords : ol.proj.transform([data.busArrival[0].arrivals[i].lon, data.busArrival[0].arrivals[i].lat], 'EPSG:4326', 'EPSG:3857')
        };
        
        primaryArray[i] = {  
            busRoute : data.busArrival[0].arrivals[i].busRoute,
            time : data.busArrival[0].arrivals[i].arrivalTime,
        };
    }
     setArrivalTimeInformation(primaryArray);
}

function handleSuccessResponse(data){
    if(data.busArrival == null){
        clearScreen();
        $('.informations').append('\
            <div class="info">\
                <span class="sidepanel-bus">НЕТ АВТОБУСОВ</span>\
            </div>\
        ');
        ErrorPageRequest();
        source.clear();
    } else {
        busMovement(data);
        setInfo();
        addMarkerCluster();
    } 
}

arrivalRequest();

function arrivalRequest(){
    $.ajax({
        type: "POST",
        crossOrigin: true,
        url: "http://passenger.vistar.su/VPArrivalServer/arrivaltimeslist",
        data: JSON.stringify({
            regionId: 36,
            fromStopId: [id],
	        toStopId: [id]
        }),
        success: handleSuccessResponse,
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        error: error
      });
}