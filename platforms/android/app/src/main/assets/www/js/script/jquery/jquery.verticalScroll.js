$.fn.verticalMarquee = function(options) {
    var self = this;
    var $children = self.children();
    var i = 0;
    var $childrenHeight = (function() {
        var height = 0;
        $children.each(function() {
            height += $(this).height();
        });
        return height;
    }());

    var defaults = {
        spacing: 0
    };

    var config = $.extend(defaults, options);
    
    self.doScroll = function($el) {
        var offsetTop = getOffsetTop();

        $el.transition({
            y: (parseFloat(offsetTop) - 25)
        }, 1000, 'linear', function() { // 700

            var isInvisible = (getOffsetTop() + parseFloat($(this).css('top')) + $(this).height()) < 0;

            if (isInvisible) pushDown($(this));

            self.doScroll($(this));
        });

        function getOffsetTop() {
            return (parseFloat($el.css('transform').split(',')[5])) || 0;
        }

        function pushDown($el) {
            var newTop = parseFloat($el.css('top'));
            newTop += $childrenHeight;
            newTop += config.spacing * $children.length;
            $el.css('top', newTop);
        }
    };

    $children.each(function() {
        var $this = $(this);
        $this.css("top", i); 
        i += $this.height() + config.spacing; 
        self.doScroll($this);
    });
    
    return this;
};