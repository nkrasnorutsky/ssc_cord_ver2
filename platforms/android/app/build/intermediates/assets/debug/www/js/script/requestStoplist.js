var map;

function error() {
    $('body').html('<h1>Ошибка<h1>');
    setInterval(function(){
      location.reload();
    }, 60000);
}

function setTitle(name, comment){
    $('.name').text((name).toUpperCase());
    $('.desc').text(comment);  
}

function initMap(lonlat){

    var iconFeature = new ol.Feature({
        geometry: new ol.geom.Point(lonlat),
        population: 4000,
        rainfall: 500
      });
      
      var iconStyle = new ol.style.Style({
        image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
          anchor: [0.5, 100],
          anchorXUnits: 'fraction',
          anchorYUnits: 'pixels',
          opacity: 1,
          src: 'img/location.png'
        }))
      });
      
      iconFeature.setStyle(iconStyle);
      
      var vectorSource = new ol.source.Vector({
        features: [iconFeature]
      });
      
      var vectorLayer = new ol.layer.Vector({
        source: vectorSource
      });
      
      var rasterLayer = new ol.layer.Tile({
        source: new ol.source.TileJSON({
          url: 'http://api.tiles.mapbox.com/v3/mapbox.geography-class.jsonp'
        })
      });
    setCenterAndZoom();
    map = new ol.Map({
    view: new ol.View({
       center: lonlat,
       zoom: 15.5
     }),
   controls: ol.control.defaults().extend([
       new ol.control.FullScreen({
         source: 'fullscreen'
       })
     ]),
     layers: [
       new ol.layer.Tile({
         source: new ol.source.OSM()
       }), rasterLayer, 
          // new ol.layer.Vector({ // реклама
          //   source: markerSource,
          //   style: markerStyle  }),
           vectorLayer, clusters
     ],
     target: 'map',
   });
}

function handleStopListsResponse(data){
    var lonlat = ol.proj.transform([data.stops[id].lon, data.stops[id].lat], 'EPSG:4326', 'EPSG:3857');
    initMap(lonlat);
    setTitle(data.stops[id].name, data.stops[id].comment);
}

$(document).ready(function(){
    $.ajax({
        type: "POST",
        crossOrigin: true,
        url: "http://passenger.vistar.su/VPArrivalServer/stoplist",
        data: JSON.stringify({regionId: 36}),
        success: handleStopListsResponse,
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        error: error
      });
});