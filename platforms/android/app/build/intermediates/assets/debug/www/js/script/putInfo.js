function putInfo(info){
    for (var i = 0; i < info.length; i++) {
        if(info[i].nextTime != undefined){
        $('.informations').append('\
        <li class="info">\
                <span class="sidepanel-bus">'+ info[i].busRoute +'</span>\
                <div class="time">\
                    <span class="sidepanel-time">'+ getMinutes(info[i].nearTime) +' <span class="min">Мин.</span></span>\
                    <span class="sidepanel-time-next">'+ getMinutes(info[i].nextTime) +' <span class="min">Мин.</span></span>\
                </div>\
            </li>\
        '); 
        } else {
            $('.informations').append('\
        <li class="info">\
                <span class="sidepanel-bus">'+ info[i].busRoute +'</span>\
                <div class="time">\
                    <span class="sidepanel-time">'+ getMinutes(info[i].nearTime) +' <span class="min">Мин.</span></span>\
                    <span class="sidepanel-time-next">  &#8212 <span class="min"></span></span>\
                </div>\
            </li>\
        ');
        }
    }
}