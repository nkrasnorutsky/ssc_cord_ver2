// TODO: вывод рекламы на карту
// новый слой с рекламой
// один слой для каждой рекламы
// вызываем функцию addMarker();
// смотреть как было раньше
// прогноз погоды возможно бегущая строка
// бегущая строка от начала карты до логотипа
// реклама-маркер
// реклама вместо карты
// координаты офиса - 51.667360, 39.195067

var markerSource = new ol.source.Vector();


addAdvertisingMarker();

function addAdvertisingMarker(){
  
 markerStyle = [
           new ol.style.Style({
            image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
              anchor: [0.05, 180],
              anchorXUnits: 'fraction',
              anchorYUnits: 'pixels',
              opacity: 1,
              src: 'img/a_left.png'
          }))
        }), 
          new ol.style.Style({
            text: new ol.style.Text({
                text: "VIstar\nПишем программы, нужные людям",
                offsetY: -100,
                offsetX: 175,
                font: 'bold 20px Rubik'
            })
          })
        ]
     
    var iconFeatures = [];

    var iconFeature = new ol.Feature({
        geometry: new ol.geom.Point(ol.proj.transform([39.195067, 51.667360], 'EPSG:4326', 'EPSG:3857')),
        population: 4000,
        rainfall: 500
    });

    iconFeature.setStyle(markerStyle);

    markerSource.addFeature(iconFeature);
}

function OffAdvertising() {
    $(".advertising").css({"visibility": "hidden"});
}

function OnAdvertising() {
    $(".advertising").css({"visibility": "visible"});
}

    // var coords = ol.proj.transform([39.195067, 51.667360], 'EPSG:4326', 'EPSG:3857');
    // var text = 'Пишем программы, нужные людям';
    // var img = '../img/icon_v.png';
    // addAdvertisingMarker(coords, text, img);
