function sortBusRoutes(){
    for(var i = 0; i < info.length; i++){
        info[i].order = parseInt(info[i].busRoute.replace(/\D+/g,""));
    }
    for(var i = info.length - 1; i >= 0; i--){
        for(var j = 1; j <= i; j++){
            if(info[j-1].order > info[j].order){
                var temp = info[j-1];
                info[j-1] = info[j];
                info[j] = temp;
            }
        }
    }
}