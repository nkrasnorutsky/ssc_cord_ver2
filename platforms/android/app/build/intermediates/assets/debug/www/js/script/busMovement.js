var distance = 40;
var features = [];
var clusters = new ol.layer.Vector({
  source: null,
  style: function(feature) {
    return new ol.style.Style({
      image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
          anchorXUnits: 'fraction',
          anchorYUnits: 'pixels',
          opacity: 1,
          src: 'img/BM test.png'
        })),
      text: new ol.style.Text({
        text: feature.get('features').map(function(_f){
            return _f.N.name;
        }).join(', '), 
        font: 'bold 50px Rubik',
        offsetY: 70
      })
    });
  }
});

var source = new ol.source.Vector();

function addMarkerCluster(){
    features = [];
    for (var i = 0; i < markers.length; i++) {
        features[i] = new ol.Feature({
          population: 4000,
          rainfall: 500,
          name: markers[i].busRoute,
          geometry: new ol.geom.Point(markers[i].coords)
        });
      }

    source = new ol.source.Vector({
        features: features
    });
    
    var clusterSource = new ol.source.Cluster({
        distance: distance,
        source: source
    });
    
    clusters.setSource(clusterSource);    
}