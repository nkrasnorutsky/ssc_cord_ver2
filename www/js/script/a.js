var markerSource = new ol.source.Vector();

addAdvertisingMarker();

function addAdvertisingMarker(){
  
 markerStyle = [
           new ol.style.Style({
            image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
              anchor: [0.05, 180],
              anchorXUnits: 'fraction',
              anchorYUnits: 'pixels',
              opacity: 1,
              src: 'img/a_left.png'
          }))
        }), 
          new ol.style.Style({
            text: new ol.style.Text({
                text: "VIstar\nПишем программы, нужные людям",
                offsetY: -100,
                offsetX: 175,
                font: 'bold 20px Rubik'
            })
          })
        ]
     
    var iconFeatures = [];

    var iconFeature = new ol.Feature({
        geometry: new ol.geom.Point(ol.proj.transform([39.195067, 51.667360], 'EPSG:4326', 'EPSG:3857')),
        population: 4000,
        rainfall: 500
    });

    iconFeature.setStyle(markerStyle);

    markerSource.addFeature(iconFeature);
}

function OffAdvertising() {
    $(".advertising").css({"visibility": "hidden"});
}

function OnAdvertising() {
    $(".advertising").css({"visibility": "visible"});
}
